Better Passwords 8.x-1.x
------------------------


Better Passwords 8.x-1.0-beta2, 2017-09-04
------------------------------------------
- Fixed #2905549: Better Passwords should not validate empty password fields
- Fixed #2904939 by dnotes: Provide means of generating initial passwords
- Added schema for better passwords config


Better Passwords 8.x-1.0-beta1, 2017-08-27
------------------------------------------
- Initial release of Better Passwords
